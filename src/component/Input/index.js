import React from 'react'
import { StyleSheet, Text, View } from 'react-native'
import { TextInput } from 'react-native-gesture-handler'

const Input = ({placeholder, value, onChangeText, securityTextEntry}) => {
    return (
       <TextInput 
       placeholder={placeholder}
       value={value}
       onChangeText={onChangeText}
       securityTextEntry={securityTextEntry}
       style={styles.input}/>
    )
}

export default Input;

const styles = StyleSheet.create({
    input:{
        borderWidth:2,
        width:280,
        height:48,
        borderRadius:5,
        borderColor: '#EA2027',
        alignItems:'center',
        alignContent:'center',
        color:'black',
    }

})
