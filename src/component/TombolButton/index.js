import React from 'react'
import { StyleSheet, Text, View } from 'react-native'
import { TouchableOpacity } from 'react-native-gesture-handler'

const TombolButton = ({title, onPress}) => {
    return (
        <TouchableOpacity onPress={onPress} style={styles.Button}>
            <Text style={styles.text}>{title}</Text>
        </TouchableOpacity>
    )
}

export default TombolButton;

const styles = StyleSheet.create({
    Button:{
        backgroundColor:'#EA2027',
        borderRadius:4, 
        paddingVertical:12, 
        paddingHorizontal:18,
        width:280,
        height:48,
        alignItems:'center',
        justifyContent:'center',
    },
    text:{
        textTransform:'uppercase',
        fontWeight:'bold',
        color:'white',
        fontSize:16,
    }
})
