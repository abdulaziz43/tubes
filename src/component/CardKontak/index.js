import React from 'react'
import { View, Text,StyleSheet, Image } from 'react-native'
import { TouchableOpacity } from 'react-native-gesture-handler';
import edit from '../../assets/Close.jpg';
import Close from '../../assets/edit.jpg';

const CardKontak = ({id, kontakItem, navigation, removeData}) => {
    return (
    <View style={styles.image} >
        <View>
        <TouchableOpacity style={styles.container} onPress={()=>navigation.navigate('DetailList', {id: id})}>
            <View>
                <Text style={styles.nama}>{kontakItem.nama}</Text>
                <Text style={styles.noHp}>{kontakItem.nomorHp}</Text>
                <Text>{kontakItem.alamat}</Text>
            </View>
        </TouchableOpacity>
        </View>

        <View>
        <TouchableOpacity onPress={() => removeData(id)}>
            <Image source={edit} style={{width:40, height:40, borderRadius:20}} />
            </TouchableOpacity>
            
            <TouchableOpacity onPress={() => navigation.navigate('EditKontak')} >
            <Image source={Close} style={{width:40, height:40, borderRadius:20}} />
        </TouchableOpacity>
        </View>

       
    

    </View>
    )
}

export default CardKontak;

const styles = StyleSheet.create ({
    container:{
        flexDirection:'row',
        padding:15,
        width:300,
        height:100,
        backgroundColor:'#EA2027',
        borderRadius:5,
        marginBottom:20
    },
    image :{
        flexDirection:'row',
        justifyContent:'space-between',
        alignItems:'center',
        alignContent:'center'
    },
    nama:{
        fontWeight:'bold',
        fontSize:16,
        color:'black'

    },
    noHp:{
        fontSize:12,
        color:'black',

    },
})
