import React from 'react'
import { View, Text, Image } from 'react-native'
import { TouchableOpacity } from 'react-native-gesture-handler'

const NavigationButton = (props, title) => {
    return (
        <View style={{flex:1, alignItems:'center', justifyContent:'center'}}>
            <TouchableOpacity onPress={props.onPress}>
                <Image style={{width:40, height:30}} source={props.img}/>
                <Text style={{color:'white'}} title={title}/>
            </TouchableOpacity>
        </View>
    )
}

export default NavigationButton;