import InputData from './InputData';
import CardKontak from './CardKontak';
import Input from './Input';
import TombolButton from './TombolButton';
import TextButton from './TextButton';
import OtherButton from './OtherButton';
import NavigationButton from './NavigationButton'

export {InputData, CardKontak, Input, TombolButton, TextButton, OtherButton, NavigationButton}