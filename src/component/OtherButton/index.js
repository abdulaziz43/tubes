import React from 'react';
import {StyleSheet, Image, View} from 'react-native';
import {TouchableOpacity} from 'react-native-gesture-handler';


const OtherButton = ({onPress, title, img}) =>{
    return(
        <View style={styles.tes}>
            <TouchableOpacity style={styles.tes} onPress={onPress} title={title} >
            <Image style={{width:48, height:40, alignContent:'center', alignItems:'center'}} source={img}/>
            </TouchableOpacity>
        </View>
    )
};

const styles=StyleSheet.create({
    tes:{
        backgroundColor:'white', 
        width:48,
        height:48,
        borderRadius:4,
        flexDirection:'row',
        alignContent:'center',
        alignItems:'center',
        
    
    }

});

export default OtherButton;