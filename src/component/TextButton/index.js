import React from 'react'
import { StyleSheet, Text, View } from 'react-native'
import { TouchableOpacity } from 'react-native-gesture-handler'

const TextButton = ({onPress}) => {
    return (
        <TouchableOpacity onPress={onPress}>
            <Text style={styles.text}>Don't have account? Click here to signup</Text>
        </TouchableOpacity>
    )
}

export default TextButton

const styles = StyleSheet.create({
    text:{
        color:'#EA2027',
    }

})
