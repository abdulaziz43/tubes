import firebase from 'firebase';

firebase.initializeApp({
    apiKey: "AIzaSyCktH7nP9xtnyRGwssOZecBcOEVHhCrJjo",
    authDomain: "crud-react-native-9ab5c.firebaseapp.com",
    databaseURL: "https://crud-react-native-9ab5c.firebaseio.com",
    projectId: "crud-react-native-9ab5c",
    storageBucket: "crud-react-native-9ab5c.appspot.com",
    messagingSenderId: "102973819593",
    appId: "1:102973819593:web:d6c3e126f34ff95d0adb18",
    measurementId: "G-ZDYJX397ZH"
})

const FIREBASE = firebase;

export default FIREBASE;