import Firebase from '../utils/Firebase';
import { FirebaseProvider, withFirebaseHOC } from '../utils/FirebaseContex';

export default Firebase

export { FirebaseProvider, withFirebaseHOC };