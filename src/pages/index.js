import ListPrint from './ListPrint';
import TambahKontak from './TambahKontak';
import DetailList from './DetailList';
import Login from './Login';
import Register from './Register';
import EditKontak from './EditKontak';
import NavBar from './NavBar';
import Transfer from './Transfer';
import UploadImage from './UploadImage';
import Profile from './Profile';
import HitamPutih from './HitamPutih';
import PilihanRegister from './PilihanRegister';
import Notif from './Notif';

export {ListPrint, TambahKontak, 
    DetailList, Login, Register, 
    EditKontak, NavBar,Transfer, 
    UploadImage, Profile, 
    HitamPutih, PilihanRegister,
    Notif
}