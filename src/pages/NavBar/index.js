import React from 'react';
import { View, Text, StyleSheet, Image } from 'react-native';
import Document from '../../assets/Document.jpg';
import Banner from '../../assets/Banner.jpg';
import Gambar from '../../assets/Gambar.jpg';
import OtherButton from '../../component/OtherButton';
import Desa from '../../assets/Desa.jpg';

const NavBar = ({navigation}) => {
    return (
    <View>
        <View style={{flex:1, marginBottom:615}}>

            <View>
                <Image source={Desa} style={{height:200,borderBottomLeftRadius:15,borderBottomRightRadius:15}}>

                </Image>
            </View>

            <View style={{padding:120}}/>
                
                <View style={{justifyContent:'center'}}>
                        <Text style={{fontSize:13, fontWeight:'bold', marginLeft:45}}>
                            Silahkan Pilih Menu Print
                        </Text>
                    
                    <View style={{alignContent:'center', alignItems:'center', backgroundColor:'#E5E5E5'}}>

                        <View style={styles.utama}>

                            <View style={{flex:1, marginHorizontal:20, marginLeft:25, alignItems:'center'}}>
                            <OtherButton onPress={ () => navigation.navigate('HitamPutih')} img={Document}/>
                            <Text style={{color:'white'}}>
                                FilePDF
                            </Text>
                            </View>

                            <View style={{flex:1, marginHorizontal:20, marginLeft:25, alignItems:'center'}}>
                            <OtherButton onPress={ () => navigation.navigate('DropDwon')} img={Banner}/>
                            <Text style={{color:'white'}}>
                                Banner
                            </Text>
                            </View>

                            <View style={{flex:1, marginHorizontal:20, marginLeft:25, alignItems:'center'}}>
                            <OtherButton onPress={ () => navigation.navigate('DropDwon')} img={Gambar}/>
                            <Text style={{color:'white'}}>
                                Image
                            </Text>
                            </View> 
                            
                        </View>     
                    </View> 
                </View>  
            </View>
    </View>
    );
};


const styles=StyleSheet.create({
    utama:{
        height:89, 
        width:320,
        borderRadius:10, 
        backgroundColor:'#EA2027',
        alignItems:'center', 
        alignContent:'center',
        justifyContent:'center',
        textShadowColor:'white',
        flexDirection:'row',

    },
    text:{
        height:42, 
        width:297,
    }
});
export default NavBar;
