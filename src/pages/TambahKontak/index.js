import React, { Component } from 'react'
import { View, Text, StyleSheet, Alert, Image, TouchableOpacity } from 'react-native'
import { onChange } from 'react-native-reanimated';
import { InputData, TombolButton } from '../../component/index';
import FIREBASE from '../../config/FIREBASE/index';
import DropDownPicker from 'react-native-dropdown-picker';
// import ImagePicker from 'react-native-image-picker'
// import DocumentPicker from 'react-native-document-picker';

import ImagePicker from 'react-native-image-picker';
import storage from '@react-native-firebase/storage';
import { Button } from 'react-native-ui-kitten';
import { ScrollView } from 'react-native-gesture-handler';
import { withFirebaseHOC } from '../../utils/index';



export default class TambahKontak extends Component {
    constructor(props) {
        super(props)

        this.state = {
            // nama: '',
            nomorHp: '',
            alamat: '',
            resourcePath:'',
            imageName:'',
            uploadUri:'',
            jumlahPrint:'',
            jenisKertas:'',
            filePath:'',
            fileName:'',
            fileUrl:'',

        };
    }

    onChangeText = (namaState, value) => {
        this.setState({
            [namaState]: value,
        });
    };

    selectImage = () => {
        const options = {
          noData: true
        }
        ImagePicker.launchImageLibrary(options, response => {
          if (response.didCancel) {
            console.log('User cancelled image picker')
          } else if (response.error) {
            console.log('ImagePicker Error: ', response.error)
          } else if (response.customButton) {
            console.log('User tapped custom button: ', response.customButton)
          } else {
            const source = { uri: response.uri }
            console.log(source)
            this.setState({
              image: source
            })
            this.setState({imgpath: "file:/" + response.path});
            console.log(this.state.imgpath);
            this.setState({imgname: response.fileName});
            console.log(this.state.imgname);
            this.setState({urldownload:"gs://dti-tubes.appspot.com/"+ response.fileName});
            console.log(this.state.urldownload);
          }
        })
    }

    onSubmit = async () => {
        const uri = this.state.imgpath;
        const filename = this.state.imgname;
        // const uploadUri = this.state.imgpath;
        const uploadUri = Platform.OS === 'ios' ? uri.replace('file://', '') : uri;
        const task = storage()
          .ref(filename)
          .putFile(uploadUri);
        try{
            await task;
        }
        catch (e) {
            console.error(e)
        }
        //console.log(uploadUri);
        // if(this.state.nama && this.state.nomorHp && this.state.alamat && this.state.jumlahPrint && this.state.jenisKertas){
    
        if(this.state.nomorHp && this.state.alamat && this.state.jumlahPrint && this.state.jenisKertas && this.state.imgpath && this.state.imgname && this.state.urldownload){
        // console.log('Masuk Submit');
        // console.log(this.state);
        const kontakReferensi = FIREBASE.database().ref('Kontak');
        const kontak = {
            // nama : this.state.nama,
            nomorHp : this.state.nomorHp,
            alamat : this.state.alamat,
            jumlahPrint : this.state.jumlahPrint,
            jenisKertas : this.state.jenisKertas,
            filePath : this.state.imgpath,
            fileName : this.state.imgname,
            fileUrl : this.state.urldownload,
            // resourcePath : this.state.resourcePath,
            // imageName : this.state.imageName,
            // uploadUri : this.state.uploadUri
        }
        
        kontakReferensi
            .push(kontak)
            .then((data)=>{
                Alert.alert('Sukses', 'Kontak tersimpan');
                this.props.navigation.replace('ListPrint');
            })
            .catch((error)=>{
                console.log("Error:", error);
            })

        }else{
            Alert.alert('Error', 'Nama, Nomor Hp dan Alamat wajib diisi..!!!');
        }

    };
    

    render() {
        return (
        <ScrollView>
            <View style={styles.pages}>

                {/* <InputData
                    label="Nama :"
                    placeholder="Masukkan nama"
                    onChangeText={this.onChangeText}
                    value={this.state.nama}
                    namaState="nama"
                /> */}
                 <View style={{paddingTop:15}}/>
                <InputData
                    label="No hp :"
                    placeholder="Masukkan nomor hp"
                    keyboardType="number-pad"
                    onChangeText={this.onChangeText}
                    value={this.state.nomorHp}
                    namaState="nomorHp"
                />
                <View style={{paddingTop:15}}/>
                 <InputData
                    label="Jumlah Print :"
                    placeholder="Masukkan Jumlah Print"
                    keyboardType="number-pad"
                    onChangeText={this.onChangeText}
                    value={this.state.jumlahPrint}
                    namaState="jumlahPrint"
                />

                <View style={{paddingTop:15}}>
                <Text style={{fontSize:16, paddingBottom: 5}}>Pilih Jenis Kertas: </Text>
                <DropDownPicker
                    items={[
                        {label: 'A2', value: 'a2'},
                        {label: 'A3', value: 'a3'},
                        {label: 'A4', value: 'a4'},
                    ]}
                    placeholder="Silahkan Pilih Jenis Kertas"
                    containerStyle={{height: 40}}
                    onChangeItem={item => this.setState({jenisKertas: item.value})}
                />
                </View>
                <View style={{paddingTop:15}}/>
                <InputData
                    label="Alamat: "
                    placeholder="Masukkan Alamat"
                    isTextArea={true}
                    onChangeText={this.onChangeText}
                    value={this.state.alamat}
                    namaState="alamat"
                />
               <Button
                    onPress={this.selectImage}
                        style={{
                        alignItems: 'center',
                        padding: 10,
                        margin: 30
                    }}>
                    Add an image
                </Button>
                <View style={styles.button}>
                    <TombolButton 
                    title="Submit"  
                    onPress={()=> this.onSubmit()}/>
                </View>
            </View>
        </ScrollView>
        );
    }
}

const styles = StyleSheet.create({
    pages: {
        flex: 1,
        padding: 50,
        backgroundColor:'white'
    },
    button:{
        alignItems:'center',
        paddingTop:10,
    },
    button1: {
        width: 250,
        height: 60,
        backgroundColor: '#3740ff',
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 4,
        marginBottom:12    
    }
})
