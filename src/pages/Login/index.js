// import React from 'react'
// import { StyleSheet, Text, View, Image } from 'react-native'
// import Input from '../../component/Input';
// import TombolButton from '../../component/TombolButton';
// import kuyprint from '../../assets/kuyprint.jpg';
// import TextButton from '../../component/TextButton';


// const Login = ({navigation}) => {
//     return (
//         <View style={styles.halamanpertaman}>
//             <Image source={kuyprint} style={{width:250, height:250,}}/>
//             <View style={styles.view}/>
//             <Input placeholder="E-Mail"/>

//             <View style={styles.view}/>
//             <Input placeholder="Password"/>
            
//             <View style={styles.view}/>
//             <TombolButton title="Login" onPress={()=> navigation.navigate('Router')}/>

//             <View style={styles.view}/>
//             <TextButton onPress={()=> navigation.navigate('PilihanRegister')}/>

//         </View>
//     )
// }

// export default Login

// const styles = StyleSheet.create({
//     halamanpertaman:{
//         justifyContent:'center', 
//         flex:1, 
//         alignItems:'center', 
//         backgroundColor:'white'
        
//     }, 
//     view:{
//         paddingTop:10,
//     }
// })

import React, { Component } from 'react';
import { StyleSheet, Text, View, TextInput, Button, Alert, ActivityIndicator, Image } from 'react-native';
import firebase from '../../config/FIREBASE/index';
import TombolButton from '../../component/TombolButton/index';
import TextButton from '../../component/TextButton/index';
import kuyprint from '../../assets/kuyprint.jpg';


export default class Login extends Component {
  
  constructor() {
    super();
    this.state = { 
      email: '', 
      password: '',
      isLoading: false
    }
  }

  updateInputVal = (val, prop) => {
    const state = this.state;
    state[prop] = val;
    this.setState(state);
  }

  userLogin = () => {
    if(this.state.email === '' && this.state.password === '') {
      Alert.alert('Email atau Password anda salah, silahkan dicoba lagi!')
    } else {
      this.setState({
        isLoading: true,
      })
      firebase
      .auth()
      .signInWithEmailAndPassword(this.state.email, this.state.password)
      .then((res) => {
        console.log(res)
        console.log(res.user.displayName)
        console.log(res.user.uid)
        console.log(res.user.phoneNumber)
        // console.log(res.uid)
        console.log('User logged-in successfully!')
        this.setState({
          isLoading: false,
          email: '', 
          password: ''
        })
        this.props.navigation.navigate('Router')
      })
      .catch(error => this.setState({ errorMessage: error.message }))
    }
  }

 
  render() {
    if(this.state.isLoading){
      return(
        <View style={styles.preloader}>
          <ActivityIndicator size="large" color="#9E9E9E"/>
        </View>
      )
    }    
    return (
      <View style={styles.container}>  
      <Image source={kuyprint}  style={{width:250, height:250,}}/>
      <View style={{paddingTop:15}}/>
        <TextInput
          style={styles.inputStyle}
          placeholder="Email"
          value={this.state.email}
          onChangeText={(val) => this.updateInputVal(val, 'email')}
        />

        <View style={{paddingTop:15}}/>   
        <TextInput
          style={styles.inputStyle}
          placeholder="Password"
          value={this.state.password}
          onChangeText={(val) => this.updateInputVal(val, 'password')}
          maxLength={15}
          secureTextEntry={true}
        />   
        <View style={{paddingTop:15}}/>
        <TombolButton title="Login" onPress={() => this.userLogin()}/>

        <View style={{paddingTop:15}}/>
        <TextButton onPress={() => this.props.navigation.navigate('Register')} />                    
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "column",
    justifyContent: "center",
    padding: 35,
    backgroundColor:'white',
    alignContent:'center',
    justifyContent:'center',
    alignItems:'center'
  },
  inputStyle: {
    width: '100%',
    marginBottom: 15,
    paddingBottom: 15,
    alignSelf: "center",
    borderColor: "#ccc",
    borderBottomWidth: 1
  },
  loginText: {
    color: '#3740FE',
    marginTop: 25,
    textAlign: 'center'
  },
  preloader: {
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    position: 'absolute',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#fff'
  }
});