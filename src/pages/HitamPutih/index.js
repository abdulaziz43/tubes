import React from 'react'
import { StyleSheet, View, ImageBackground } from 'react-native'
import Warna from '../../assets/Warna.jpg';
import Hitam from '../../assets/Hitam.jpg';
import { TombolButton } from '../../component';


const HitamPutih = ({navigation}) => {
    return (
    <View style={styles.tampilan}>
        <View >
            <ImageBackground source={Warna} style={styles.warna}>
                <TombolButton title="Print Berwarna" onPress={ () => navigation.navigate('TambahKontak')}/>        
            </ImageBackground>
        </View>


        <View>
            <ImageBackground source={Hitam} style={styles.hitam} >
                <TombolButton title="print Hitam Putih" onPress={()=> navigation.navigate('TambahKontak')}/>
            </ImageBackground>
        </View>

    </View>
    )
}

export default HitamPutih;

const styles = StyleSheet.create({
    warna:{
        height:300,
        width:300,
        borderRadius:150,
         alignItems:'center',
        justifyContent:'center'
        
    },
    hitam:{
        height:300,
        width:300,
        borderRadius:150,
        alignItems:'center',
        justifyContent:'center'
        
    },
    tampilan:{
        flex:1,
        justifyContent:'center',
        alignItems:'center'
    }
})
