import React, { Component } from 'react'
import { Text, View, StyleSheet } from 'react-native'
import { color } from 'react-native-reanimated';
import FIREBASE from '../../config/FIREBASE';

export default class DetailList extends Component {
    constructor(props){
        super(props)

        this.state={
            kontak :{},
        }
    }

    componentDidMount(){
        FIREBASE.database()
        .ref("Kontak/"+ this.props.route.params.id)
        .once('value', (querySnapShot) => {
            let data = querySnapShot.val() ? querySnapShot.val() : {};
            let kontaksItem = {...data};

            this.setState({
                kontak: kontaksItem,
            });
        });
    }

    render() {
        const  {kontak} = this.state;
        return (
            <View style={styles.page}>
                <Text>Nama : </Text>
                <Text style={styles.text}>{kontak.nama} </Text>

                <Text>No. Hp : </Text>
                <Text style={styles.text}>{kontak.nomorHp} </Text>

                <Text>Alamat : </Text>
                <Text style={styles.text}>{kontak.alamat} </Text>
            </View>
        )
    }
}

const styles=StyleSheet.create({
    page :{
        padding:20,
        margin:30,
        backgroundColor:'#0fbcf9',
        borderRadius:10,
    },
    text :{
        fontWeight:'bold',
        fontSize:18,
        marginBottom:10,
        color:'white'
    }
    
})