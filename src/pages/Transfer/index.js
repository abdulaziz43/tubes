import React from 'react'
import { StyleSheet, Text, View } from 'react-native'
import TombolButton from  '../../component/TombolButton'


const Transfer = ({navigation}) => {
    return (
        <View style={styles.upload}>
            <View>
                <View style={{width:369, height:237, backgroundColor:'#EA2027', borderRadius:20, justifyContent:'center', alignItems:'center'}}>
                    <Text style={{color:'white'}}>
                        Untuk Pembayaran Dapat dilakukan
                    </Text>
                    <Text style={{color:'white'}}>
                        Transfer Via ATM
                    </Text>
                    <Text style={{color:'white', fontSize:20}}>
                        Nominal Transfer
                    </Text>
                    <Text style={{color:'white', fontSize:20}}>
                        Rp.20.000
                    </Text>
                    <Text style={{color:'white', fontSize:15}}>
                        Kerekening:0845 0984 8000
                    </Text>
                </View>
            </View>
            <View style={{padding:15}}>
            <TombolButton title='Upload Bukti Transfer' onPress={()=>navigation.navigate('Notif')}/>
            </View>
        </View>
    )
}

export default Transfer

const styles = StyleSheet.create({
    upload :{
        justifyContent:'center',
        alignContent:'center',
        alignItems:'center',
        flex:1,
        fontWeight:'bold'
    }
})


