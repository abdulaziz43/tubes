// import React from 'react'
// import { StyleSheet, Text, View } from 'react-native'
// import TombolButton from '../../component/TombolButton'
// import Input from '../../component/Input';


// const Register = ({navigation}) => {
//     return (
//         <View style={styles.halamankedua}>

//             <Text style={styles.text1}>
//                 Selamat Datang!
//             </Text>

//             <View style={styles.text2}>
//             <Text>
//                 Silahkan Lengkapi Data Anda 
//             </Text>
//             <Text>
//                 Untuk Proses Registrasi Akun
//             </Text>
//             </View>
            
//             <View style={styles.view}/>
//             <Input placeholder="Nama" />

//             <View style={styles.view}/>
//             <Input placeholder="E-Mail" />

//             <View style={styles.view}/>
//             <Input placeholder="Password" />

//             <View style={styles.view}/>
//             <Input placeholder="No. Hp" />

//             <View style={styles.view}/>
//             <TombolButton title="Register" onPress={()=> navigation.navigate('Login')}/>
//         </View>
//     )
// }

// export default Register

// const styles = StyleSheet.create({
//     halamankedua:{
//         justifyContent:'center',
//         alignItems:'center',
//         flex:1,
//     },
//     view:{
//         paddingTop:10,
//     },
//     text1:{
//         fontWeight:'bold',
//         fontSize:40,
//         color:'#EA2027',
//     },
//     text2:{
//         fontSize:15,
//         color:'black',
//         paddingBottom:60
//     }
// })
import React, { Component } from 'react';
import { StyleSheet, Text, View, TextInput, Button, Alert, ActivityIndicator } from 'react-native';
import firebase from '../../config/FIREBASE/index';
import TombolButton from '../../component/TombolButton';



export default class Register extends Component {
  
    constructor() {
      super();
      this.state = { 
        displayName: '',
        email: '', 
        password: '',
        isLoading: false
      }
    }
  
    updateInputVal = (val, prop) => {
      const state = this.state;
      state[prop] = val;
      this.setState(state);
    }
  
    registerUser = () => {
      if(this.state.email === '' && this.state.password === '') {
        Alert.alert('Enter details to signup!')
      } else {
        this.setState({
          isLoading: true,
        })
        firebase
        .auth()
        .createUserWithEmailAndPassword(this.state.email, this.state.password)
        .then((res) => {
          res.user.updateProfile({
            displayName: this.state.displayName,
          })
          console.log('User registered successfully!')
          this.setState({
            isLoading: false,
            displayName: '',
            email: '', 
            password: ''
          })
          this.props.navigation.navigate('Login')
        })
        .catch(error => this.setState({ errorMessage: error.message }))      
      }
    }
  
    render() {
      if(this.state.isLoading){
        return(
          <View style={styles.preloader}>
            <ActivityIndicator size="large" color="#9E9E9E"/>
          </View>
        )
      }    
      return (
        <View style={styles.container}>  
              <Text style={{fontWeight:'bold',fontSize:40, color:'#EA2027',}}>
                  Selamat Datang!
              </Text>

              <View style={{fontSize:15,color:'black',paddingBottom:70}}>
              <Text>
                  Silahkan lengkapi data anda 
              </Text>
              <Text>
                    untuk proses registrasi akun
              </Text>
             </View>
          <TextInput
            style={styles.inputStyle}
            placeholder="Name"
            value={this.state.displayName}
            onChangeText={(val) => this.updateInputVal(val, 'displayName')}
          />
          <TextInput
            style={styles.inputStyle}
            placeholder="Email"
            value={this.state.email}
            onChangeText={(val) => this.updateInputVal(val, 'email')}
          />
          <TextInput
            style={styles.inputStyle}
            placeholder="Password"
            value={this.state.password}
            onChangeText={(val) => this.updateInputVal(val, 'password')}
            maxLength={15}
            secureTextEntry={true}
          />   
          <TombolButton 
            title="Signup"
            onPress={() => this.registerUser()}/>
  
          <Text 
            style={styles.loginText}
            onPress={() => this.props.navigation.navigate('Login')}>
            Already Registered? Click here to login
          </Text>                          
        </View>
      );
    }
  }
  
  const styles = StyleSheet.create({
    container: {
      flex: 1,
      display: "flex",
      flexDirection: "column",
      justifyContent: "center",
      padding: 35,
      backgroundColor: '#fff',
      alignItems:'center'
    },
    inputStyle: {
      width: '100%',
      marginBottom: 15,
      paddingBottom: 15,
      alignSelf: "center",
      borderColor: "#ccc",
      borderBottomWidth: 1
    },
    loginText: {
      color: '#EA2027',
      marginTop: 25,
      textAlign: 'center'
    },
    preloader: {
      left: 0,
      right: 0,
      top: 0,
      bottom: 0,
      position: 'absolute',
      alignItems: 'center',
      justifyContent: 'center',
      backgroundColor: '#fff'
    }
  });