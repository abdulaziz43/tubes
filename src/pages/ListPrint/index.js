import React, { Component } from 'react';
import { Text, StyleSheet, View, Image, Alert } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import Tambah from '../../assets/Tambah.jpg';
import { CardKontak } from '../../component';
import FIREBASE from '../../config/FIREBASE';

export default class ListPrint extends Component {
    constructor(props){
        super(props)

        this.state = {
            kontaks: {},
            kontaksKey:[]
        }
    }

    componentDidMount(){
     this.ambilData();
    }

    ambilData = () => {
        FIREBASE.database()
        .ref("Kontak")
        .once('value', (querySnapShot) => {
            let data = querySnapShot.val() ? querySnapShot.val() : {};
            let kontaksItem = {...data};

            this.setState({
                kontaks: kontaksItem,
                kontaksKey : Object.keys(kontaksItem)
            })
        })

    }

    removeData= (id) =>{
        Alert.alert(
            "info",
            "Anda Yakin akan menghapus data List Print?",
            [
                {
                    text:"cancel",
                    onPress: () => console.log("cancel Pressed :", id),
                    style: "cancel"
                },
                {text:"OK", onPress:() => {
                    FIREBASE.database()
                    .ref('Kontak/'+id)
                    .remove();
                    this.ambilData();
                    Alert.alert('Hapus', 'Sukses Hapus data')
                }}
            ],
            {cancelable: false}
        );
    }


    render(){
        // console.log("Kontaks :", this.state.kontaks);
        // console.log("Kontaks :", this.state.kontaksKey);
        const {kontaks, kontaksKey} = this.state

        return(
            <View style={styles.page}>
                <View style={styles.header}>
                    <Text style={styles.title}>
                        List Print
                    </Text>
                    <View style={styles.garis}/>
                </View>

                <View style={styles.listKontak}>
                    {kontaksKey.length > 0 ?(
                        kontaksKey.map((key)=>(
                        <CardKontak 
                        key={key} 
                        kontakItem={kontaks[key]} 
                        id={key} {...this.props} 
                        removeData={this.removeData}
                        />        
                        ))
                    ):(
                        <Text>
                            Daftar Kosong
                        </Text>
                    )}

                </View>

                <View style={styles.wrapperButton}>
                    <TouchableOpacity  onPress={()=>this.props.navigation.navigate('TambahKontak')}  >
                        <Image source={Tambah} style={{width:40, height:40, borderRadius:20}}/>
                    </TouchableOpacity>
                </View>
            </View>

        );
    }
}

const styles = StyleSheet.create({
    page : {
        flex : 1,
    },
    header :{
        paddingHorizontal:30,
        paddingTop:30,
    },
    title:{
        fontSize:20,
        fontWeight:'bold',

    },
    garis:{
        borderWidth:1,
        marginTop:10,
    },
    listKontak:{
        paddingHorizontal:30,
        marginTop:20,
    },
    wrapperButton : {
        flex: 1,
        position: 'absolute',
        bottom: 0,
        right: 0,
        margin: 30,
    }
})

