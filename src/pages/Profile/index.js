import React, { Component } from 'react';
import { StyleSheet, View, Text, Button } from 'react-native';
import firebase from '../../config/FIREBASE/index';
import TombolButton from '../../component/TombolButton/index';

export default class Profile extends Component {
  constructor() {
    super();
    this.state = { 
      uid: ''
    }
  }

  signOut = () => {
    firebase.auth().signOut().then(() => {
      this.props.navigation.navigate('Login')
    })
    .catch(error => this.setState({ errorMessage: error.message }))
  }  

  render() {
    this.state = { 
      displayName: firebase.auth().currentUser.displayName,
      uid: firebase.auth().currentUser.uid
    }    
    return (
      <View style={styles.halaman}>
        <View style={styles.halamansatu}>
                <View />
                    <View style={{height:152}}/>
                        <Text style={{alignContent:"center", fontSize:18, color:"white"}}>
                          Hello.!, {this.state.displayName}
                        </Text>
                        
                    <View style={{height:9}}/>
                        <Text style={{alignContent:"center", fontSize:18, color:"white"}}>
                          UID Anda : {this.state.uid}
                        </Text>
                    </View>

                    <View style={styles.halamandua}>
                       <View style={{height:35}}/>
        
                         <View style={{height:70}}>
                         <TombolButton title="ubah profil" onPress={ () => navigation.navigate('NavBar')} />
                         </View>
        
                         <View style={{height:70}}>
                         <TombolButton title="ganti password" onPress={ () => navigation.navigate('NavBar')} />
                         </View>
                    
                        <View>
                         <TombolButton title="logout" onPress={() => this.signOut()}/>
                        </View>
                    
                     </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  halamansatu:{
    flex:2,
    backgroundColor:'#EA2027',
    alignItems:'center', 
    width:420,
    borderBottomLeftRadius:30,
    borderBottomRightRadius:30
  },
  halaman:{
    justifyContent:'center', 
    flex:1, 
    alignItems:'center', 
    backgroundColor:'#E5E5E5'
  },
  halamandua:{
    flex:3,
    backgroundColor:'#E5E5E5',
    alignItems:'center',
  }

});