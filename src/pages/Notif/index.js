import React from 'react'
import { StyleSheet, Text, View, Image} from 'react-native'
import TombolButton from  '../../component/TombolButton'
import Pemberi from '../../assets/Pemberi.jpg'


const Notif = ({navigation}) => {
    return (
        <View style={styles.upload}>
            <View style={{alignContent:'center', justifyContent:'center', alignItems:'center'}}>
                <Image source={Pemberi}/>
                <Text style={{color:'black', fontWeight:'bold', fontSize:30}}>
                    Transfer Sukses
                </Text>
                <Text style={{color:'black', fontSize:15}}>
                    20 November 2020
                </Text>
                <Text style={{color:'black', fontSize:15}}>
                    Upload foto pembayaran berhasil.!!
                </Text>
            </View>
            
            <View style={{padding:15}}>
            <TombolButton title='Done' onPress={()=>navigation.navigate('NavBar')}/>
            </View>
        </View>
    )
}

export default Notif

const styles = StyleSheet.create({
    upload :{
        justifyContent:'center',
        alignContent:'center',
        alignItems:'center',
        flex:1,
        fontWeight:'bold',
        backgroundColor:'white'
    }
})


