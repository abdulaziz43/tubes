import React from 'react'
import { View, Text } from 'react-native'
import { TombolButton } from '../../component'

const PilihanRegister = ({navigation}) => {
    return (
        <View style={{alignItems:'center',flex:1,justifyContent:'center'}}>
            <View style={{justifyContent:'center', alignItems:'center'}}>
                <TombolButton title="Printer Services" onPress={()=> navigation.navigate('Register')}/>
            </View>
            
            <View style={{justifyContent:'center', alignItems:'center', padding:30}}>
                <TombolButton title="User" onPress={()=> navigation.navigate('Register')} />
            </View>
            
        </View>
    )
}

export default PilihanRegister;
