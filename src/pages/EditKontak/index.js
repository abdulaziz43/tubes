import React, { Component } from 'react'
import { View, Text, StyleSheet, Alert } from 'react-native'
import { onChange } from 'react-native-reanimated';
import { InputData, TombolButton } from '../../component/index';
import FIREBASE from '../../config/FIREBASE/index';

export default class EditKontak extends Component {
    constructor(props) {
        super(props)

        this.state = {
            nama: '',
            nomorHp: '',
            alamat: '',
        };
    }


    onChangeText = (namaState, value) => {
        this.setState({
            [namaState]: value,
        });
    };

    onSubmit = () => {
        if(this.state.nama && this.state.alamat && this.state.alamat){
        // console.log('Masuk Submit');
        // console.log(this.state);
        const kontakReferensi = FIREBASE.database().ref('Kontak');
        const kontak = {
            nama : this.state.nama,
            nomorHp : this.state.nomorHp,
            alamat : this.state.alamat
        }
        
        kontakReferensi
            .push(kontak)
            .then((data)=>{
                Alert.alert('Sukses', 'Kontak tersimpan');
                this.props.navigation.replace('Home');
            })
            .catch((error)=>{
                console.log("Error:", error);
            })

        }else{
            Alert.alert('Error', 'Nama, Nomor Hp dan Alamat wajib diisi..!!!');
        }
    };

    render() {
        return (
            <View style={styles.pages}>
                <InputData
                    label="Nama :"
                    placeholder="Masukkan nama"
                    onChangeText={this.onChangeText}
                    value={this.state.nama}
                    namaState="nama"
                />
                <InputData
                    label="No hp :"
                    placeholder="Masukkan nomor hp"
                    keyboardType="number-pad"
                    onChangeText={this.onChangeText}
                    value={this.state.nomorHp}
                    namaState="nomorHp"
                />
                <InputData
                    label="Alamat"
                    placeholder="Masukkan Alamat"
                    isTextArea={true}
                    onChangeText={this.onChangeText}
                    value={this.state.alamat}
                    namaState="alamat"
                />
                <View style={styles.button}>
                    <TombolButton 
                    title="Submit"  
                    onPress={()=> this.onSubmit()}/>
                </View>
            </View>
        );
    }
}



const styles = StyleSheet.create({
    pages: {
        flex: 1,
        padding: 50,
    },
    button:{
        alignItems:'center',
        paddingTop:10,
    },
})
