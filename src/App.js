import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import { mapping, light as lightTheme } from '@eva-design/eva'
import { ApplicationProvider, IconRegistry } from 'react-native-ui-kitten'
import { EvaIconsPack } from '@ui-kitten/eva-icons'
import Firebase, { FirebaseProvider } from '../src/utils/index'
import SeconStack from './router/index';


const App = () => {
  return (
    <NavigationContainer>
      <ApplicationProvider mapping={mapping} theme={lightTheme}>
        <FirebaseProvider value={Firebase}>
          <SeconStack/>
        </FirebaseProvider>
      </ApplicationProvider>
    </NavigationContainer>
  )
}

export default App;
