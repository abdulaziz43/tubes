import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import TambahKontak from '../pages/TambahKontak';
import DetailList from '../pages/DetailList';
import Login from '../pages/Login';
import Register from '../pages/Register';
import EditKontak  from '../pages/EditKontak';
import NavBar from '../pages/NavBar';
import ListPrint from '../pages/ListPrint/index';
import Transfer from '../pages/Transfer';
import UploadImage from '../pages/UploadImage';
import  Profile  from '../pages/Profile';
import { BottomNavigator, HitamPutih, Notif, PilihanRegister } from '../pages';


import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';  


import Feed from '../screens/Feed'
import AddPost from '../screens/AddPost';


const Stack = createStackNavigator ();
const Tab = createBottomTabNavigator();

// const Router = () => {
//     return (
//        <Stack.Navigator>
//            <Stack.Screen name="Login" component={Login} options={{headerShown:false}}/>
//            <Stack.Screen name="Register" component={Register} />
//            <Stack.Screen name="NavBar" component={NavBar} options={{headerShown:false}}/>
//            <Stack.Screen name="ListPrint" component={ListPrint} options={{title: 'List Print',headerStyle: {backgroundColor: '#EA2027',},headerTintColor: '#fff',headerTitleStyle: {fontWeight: 'bold'}}}/>
//            <Stack.Screen name="TambahKontak" component={TambahKontak} options={{title: 'Upload File PDF',headerStyle: {backgroundColor: '#EA2027',},headerTintColor: '#fff',headerTitleStyle: {fontWeight: 'bold'}}}/>
//            <Stack.Screen name="DetailList" component={DetailList} options={{title: 'Detail Pesanan',headerStyle: {backgroundColor: '#EA2027',},headerTintColor: '#fff',headerTitleStyle: {fontWeight: 'bold'}}}/>
//            <Stack.Screen name="EditKontak" component={EditKontak} />
//            <Stack.Screen name="Transfer" component={Transfer} options={{title: 'Transfer',headerStyle: {backgroundColor: '#EA2027',},headerTintColor: '#fff',headerTitleStyle: {fontWeight: 'bold'}}}/>
//            <Stack.Screen name="UploadImage" component={UploadImage} />
//            <Stack.Screen name="Profile" component={Profile} />
//            <Stack.Screen name="BottomNavigator" component={BottomNavigator} options={{headerShown:false}}/>
//        </Stack.Navigator>
//     )
// }

// export default Router;

function MainStack(){
  return (
    <Stack.Navigator
          initialRouteName="Main"
          screenOptions={{
            headerStyle: { backgroundColor: '#42f44b' },
            headerTintColor: '#fff',
            headerTitleStyle: { fontWeight: 'bold' }
          }}>
          <Stack.Screen name="Login" component={Login} options={{headerShown:false}}/>
          <Stack.Screen name="PilihanRegister" component={PilihanRegister} options={{ title: 'Pilihan Register', headerStyle: {backgroundColor: '#EA2027'},headerTintColor: '#fff',headerTitleStyle: {fontWeight: 'bold'} }} />
          <Stack.Screen name="Register" component={Register} options={{headerShown:false}} />
        </Stack.Navigator>
  );
}

function HomeStack() {
    return (
        <Stack.Navigator
          initialRouteName="Home"
          screenOptions={{
            headerStyle: { backgroundColor: '#42f44b' },
            headerTintColor: '#fff',
            headerTitleStyle: { fontWeight: 'bold' }
          }}>
          <Stack.Screen name="NavBar" component={NavBar} options={{headerShown:false}}/>
          <Stack.Screen name="Transfer" component={Transfer} options={{ title: 'Transfer', headerStyle: {backgroundColor: '#EA2027'}, headerTintColor: '#fff',headerTitleStyle: {fontWeight: 'bold'} }} />
          <Stack.Screen name="TambahKontak" component={TambahKontak} options={{ title: 'Pesanan', headerStyle: {backgroundColor: '#EA2027'}, headerTintColor: '#fff',headerTitleStyle: {fontWeight: 'bold'}}} />
          <Stack.Screen name="HitamPutih" component={HitamPutih} options={{ title: 'Pilih Opsi', headerStyle: {backgroundColor: '#EA2027'}, headerTintColor: '#fff',headerTitleStyle: {fontWeight: 'bold'}}} />
          <Stack.Screen name="Notif" component={Notif} options={{headerShown:false}}/>
          <Stack.Screen name="DetailList" component={DetailList} options={{headerShown:false}}/>
          <Stack.Screen name="AddPost" component={AddPost} options={{headerShown:false}}/>
          <Stack.Screen name="Feed" component={Feed} options={{headerShown:false}}/>
        </Stack.Navigator>
    );
  }
  function ListStack() {
    return (
      <Stack.Navigator
        initialRouteName="List"
        screenOptions={{
          headerStyle: { backgroundColor: '#42f44b' },
          headerTintColor: '#fff',
          headerTitleStyle: { fontWeight: 'bold' }
        }}>
        <Stack.Screen name="ListPrint" component={ListPrint} options={{ title: 'Setting Page', headerStyle: {backgroundColor: '#EA2027'},headerTintColor: '#fff',headerTitleStyle: {fontWeight: 'bold'} }}/>
      </Stack.Navigator>
    );
  }

  function ProfileStack() {
    return (
      <Stack.Navigator
        initialRouteName="Profile"
        screenOptions={{
          headerStyle: { backgroundColor: '#42f44b' },
          headerTintColor: '#fff',
          headerTitleStyle: { fontWeight: 'bold' }
        }}>
        <Stack.Screen name="Profile" component={Profile} options={{ headerShown:false}}/>
      </Stack.Navigator>
    );
  }
  
  function Router() {
    return (
        <Tab.Navigator
          initialRouteName="Feed"
          tabBarOptions={{
            activeTintColor: '#EA2027',
          }}>
          <Tab.Screen
            name="HomeStack"
            component={HomeStack}
            options={{
              tabBarLabel: 'Home',
              tabBarIcon: ({ color, size }) => (
                <MaterialCommunityIcons name="home" color={color} size={size} />
              ),
            }}  />
          <Tab.Screen
            name="ListStack"
            component={ListStack}
            options={{
              tabBarLabel: 'ListPrint',
              tabBarIcon: ({ color, size }) => (
                <MaterialCommunityIcons name="badge-account-outline" color={color} size={size} />
              ),
            }} />
            <Tab.Screen
            name="ProfileStack"
            component={ProfileStack}
            options={{
              tabBarLabel: 'Porfile',
              tabBarIcon: ({ color, size }) => (
                <MaterialCommunityIcons name="account-edit-outline" color={color} size={size} />
              ),
            }} />
        </Tab.Navigator>
    );
  }
  function SeconStack(){
    return (
      <Stack.Navigator
            initialRouteName="Secon"
            screenOptions={{
              headerStyle: { backgroundColor: '#42f44b' },
              headerTintColor: '#fff',
              headerTitleStyle: { fontWeight: 'bold' }
            }}>
            <Stack.Screen name="MainStack" component={MainStack}options={{headerShown:false}} />
            <Stack.Screen name="Router" component={Router} options={{headerShown:false}}/>
          </Stack.Navigator>
    );
  }
export default SeconStack;


