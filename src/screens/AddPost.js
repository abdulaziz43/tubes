import React, { Component } from 'react'
import { Image, View, ScrollView } from 'react-native'
import { Text, Button, Input } from 'react-native-ui-kitten'
import ImagePicker from 'react-native-image-picker'
import storage from '@react-native-firebase/storage';
import { withFirebaseHOC } from '../utils';
import { TombolButton } from '../component'

class AddPost extends Component {
    state = {
      image: null,
      title: ''
    }
    onChangeTitle = title => {
      this.setState({ title })
    }
  
    selectImage = () => {
        const options = {
          noData: true
        }
        ImagePicker.launchImageLibrary(options, response => {
          if (response.didCancel) {
            console.log('User cancelled image picker')
          } else if (response.error) {
            console.log('ImagePicker Error: ', response.error)
          } else if (response.customButton) {
            console.log('User tapped custom button: ', response.customButton)
          } else {
            const source = { uri: response.uri }
            console.log(source)
            this.setState({
              image: source
            })
            this.setState({imgpath: "file:/" + response.path});
            console.log(this.state.imgpath);
            this.setState({imgname: response.fileName});
            console.log(this.state.imgname);
            this.setState({urldownload:"gs://dti-tubes.appspot.com/"+ response.fileName});
            console.log(this.state.urldownload);
          }
        })
    }
    
    onSubmit = async () => {
        const uri = this.state.imgpath;
        const filename = this.state.imgname;
        // const uploadUri = this.state.imgpath;
        const uploadUri = Platform.OS === 'ios' ? uri.replace('file://', '') : uri;
        console.log(uploadUri);
        const task = storage()
          .ref(filename)
          .putFile(uploadUri);
        try {
          await task;
          const post = {
            photo: this.state.image,
            title: this.state.title,
            link: this.state.urldownload
          }
          this.props.firebase.uploadPost(post)
      
          this.setState({
            image: null,
            title: '',
            description: ''
          })
          console.log("upload success");
        } catch (e) {
          console.error(e)
        }
    }

    render() {
        return (
          <ScrollView>
            <View style={{ flex: 1, marginTop: 60 }}>
            <View>
            {this.state.image ? (
            <Image
            source={this.state.image}
            style={{ width: 250, height: 250, alignSelf: 'center'}}
            />
            ) : (
            <Button
            onPress={this.selectImage}
            style={{
                alignItems: 'center',
                padding: 10,
                margin: 30
                }}>
            Add an image
            </Button>
            )}
            </View>
            <View style={{ marginTop: 80, alignItems: 'center' }}>
            <Text category='h4'>Bukti Pembayaran</Text>
            <Input
            placeholder='Tulis Keterangan'
            style={{ margin: 20 }}
            value={this.state.title}
            onChangeText={title => this.onChangeTitle(title)}
            />
            <Button status='success' onPress={this.onSubmit} >
            Add post
            </Button>
            </View>
            <View style={{padding:15}}>
              <TombolButton title="next page " onPress={()=>this.props.navigation.navigate('Feed')}/>
           </View>
            </View>
            </ScrollView>
        )
    }
}
       
export default withFirebaseHOC(AddPost)